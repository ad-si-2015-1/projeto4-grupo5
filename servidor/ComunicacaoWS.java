package servidor;

import memoria.Tabuleiro;
import javax.jws.*;

@WebService(serviceName = "ComunicacaoWS")

public class ComunicacaoWS {

    private static int mostrar = 99;
    private static int click = 0;

    private static boolean vez = false;

    private static Tabuleiro tabuleiro;

    public ComunicacaoWS() {
        tabuleiro = new Tabuleiro();
    }
    
    public void tratar(String texto) { //Trata o protocolo de comunicação.

        if (texto.contains("jogada")) {
            texto = texto.replace("jogada ", "");
            click = Integer.parseInt(texto); //Guarda a área clicada.
            mostrar = click;
        } else if (texto.contains("vez")) { //Controla a vez.
            if (vez == false) {
                vez = true;
            } else {
                vez = false;
            }
            mostrar = -1;
        } else if (texto.contains("Ponto!")) {
            mostrar = -2; //Quer dizer que oponente pontuou.
        }
    }

    public int getMostrar() {
        return mostrar;
    }

    public int getClick() {
        return click;
    }

    public void setMostrar() { //Reseta mostrar para um valor inútil.
        mostrar = 99;
    }

    public void setVez() {
        vez = false;
    }

    public boolean getVez() {
        return vez;
    }

    public Tabuleiro getTabuleiro() {
        return tabuleiro;
    }
}
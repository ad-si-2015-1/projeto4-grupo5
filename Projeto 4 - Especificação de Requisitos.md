##### Documento de Especificação de Requisitos

##### Jogo da Memória Multiusuário em Web Services

##### Universidade Federal de Goiás - UFG

##### Instituto de Informática - INF

##### Sistemas de Informação

##### Aplicações Distribuídas

##### 1º semestre/ 2015

##### Professor:

- Marcelo Akira Inuzuka

##### Grupo:

- Allan Braga

- Farid Chaud

- Mauro Henrique

- Miriã Laís


1.Introdução

1.1. Propósito do documento de requisitos

Este documento tem como propósito especificar as funcionalidades, requisitos funcionais, não funcionais e seus respectivos de uso, para fins de avaliação de viabilidade para futuro desenvolvimento.


1.2. Escopo do produto

O Jogo da Memória tem consiste em encontrar "peças" semelhantes, ou seja, o jogador deve selecionar duas peças, com o intuito de que elas sejam idênticas, sendo que há pares de cada peça.


1.3. Definições, acrônimos e abreviações

RF - Requisitos Funcionais;
RNFP - Requisitos não Funcionais do Produto;
RNFO - Requisitos não Funcionais Organizacionais;
RNFE - Requisitos não Funcionais Externos;
CSU - Caso de Uso.


2.Descrição Geral

2.1. Perspectiva do Software

O software deve funcionar tanto em ambiente Windows, quanto Linux. É contituído pelos relacionamentos de seis agentes, que se comunicam por meio da tecnologia RMI.


2.2. Funções do Sofware

- Identificar jogadores;
- Identificar as peças;
- Permitir jogadas sucessivas de um mesmo jogador em caso de acertos;
- Passar a vez para o outro jogador, caso as peças sejam distintas;
- Exibir pontuação do jogador e do adversário;
- Exibir vencedor.


2.3. Características dos Jogadores

Quaisquer pessoas que tenham interesse.


2.4. Restrições Gerais

- Jogo depende de uma IDE.


2.5. Melhoria do Jogo

- Matriz 4x6, aumentando o número de peças;
- Criação de uma versão inicial para a interface gráfica.


3.Requisitos do Sistema

3.1. Requisitos Funcionais (RF)

Os Requisitos Funcionais (RF) descrevem as funcionalidades e serviços do jogo, serão documentados a seguir os principais requisitos funcionais da aplicação.

RF001 - Utilizar tecnologia Web Services.

RF002 - Utilizar protocolo SOAP.

RF003 - Deve haver dois jogadores conectados.

RF004 - Jogo com matriz 4x6 com imagens, cujas posições são geradas aleatoriamente.

RF005 - Cada jogador deve receber a jogada (escolha da peça) do outro.

RF006 - Quando um jogador acertar, deve ter sua pontuação somada.

RF007 - Quando um jogador acertar, deve jogar novamente.

RF008 - Quando um jogador errar, deve passar a vez.

RF009 - O jogo deve exibir a pontuação de cada jogador, para ambos os jogadores.

RF010 - Quando todas as peças forem encontradas, vence o que tiver encontrado o maior número de pares.

RF011 - O sistema deve mostrar os pontos dos jogadores na medida em que forem sendo incrementados.

RF012 - O jogador que se conecta ao servidor primeiro, é o que inicia a partida.

RF013 - O jogador que dá inicio à partida, só pode enviar a primeira tentativa quando o adversário também já estiver conectado.


3.2. Requisitos não Funcionais (RNF)

Requisitos não funcionais diferentemente dos funcionais, não especifica o que o sistema fará, mas como ele fará. Abordam aspectos de qualidade importantes em um sistema de software. Nesta seção iremos identificar os principais requisitos não funcionais da aplicação:

3.2.1. Requisitos Não Funcionais do Produto (RNFP)

RNFP001 - O sistema apresentará uma interface gráfica, utilizando os próprios recursos do Java.

RNFP002 - O sistema deve informar aos jogadores quando é a sua vez de jogar ou de passar a vez.


3.2.2. Requisitos Não Funcionais Organizacionais (RNFO)

RNFO001 - A aplicação será desenvolvida em linguagem Java.

RNFO002 - A aplicação será desenvolvida na IDE Netbeans.

RNFO003 - A documentação do projeto será realizada em paralelo ao desenvolvimento.


3.2.3. Requisitos Não Funcionais Externos (RNFE)

RNFE001 - Uso do GitLab como ferramenta colaborativa do projeto.

RNFE002 - Uso da ferramenta de diagramação Gliffy (Site: https://www.gliffy.com).

RNFO003 - Uso da ferramenta de diagramação Dia (Site: http://dia-installer.de/)


4.Casos de Uso (CSU)

4.1. Atores

- Jogador 1;
- Jogador 2.


4.2. Casos de Uso

ARQUIVO ANEXADO.


5.Diagramas

5.1. Diagrama de Raias

ARQUIVO ANEXADO.


5.2. Diagrama de Estados

ARQUIVO ANEXADO.


AO FINAL DO PROJETO TODA A DOCUMENTAÇÃO SERÁ ADICIONADA À WIKI DO PROJETO.
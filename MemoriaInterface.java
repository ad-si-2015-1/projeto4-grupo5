package memoria;

import wserver.*;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class MemoriaInterface extends javax.swing.JFrame implements Runnable {

    public MemoriaInterface() //Inicia a interface.
    {
        initComponents();
    }

    public void run() //Thread
    {
        int x = 0;

        while (true) {

            //Imprime no console a vez. Se 0, não é a sua vez. Se 1, é a sua vez.             
            System.out.println("Vez: " + vezDoJogador);

            while (vezDoJogador == 0) {

                vezLabel.setText("Vez do oponente");

                try {
                    if (conexao.getVez() == true && souServer == 2) {
                        vezDoJogador = 1;
                        vezLabel.setText("Sua vez");
                        conexao.setMostrar(); //Reseta o valor de mostrar.

                        JOptionPane.showMessageDialog(null, "Sua vez!");

                        /* Se mudou de vez, quer dizer que o oponente errou, 
                        e as mudanças na interface devem ser desfeitas. */
                        desfaser(pontuar[0]);
                        desfaser(pontuar[1]);
                    } else if (conexao.getVez() == false && souServer == 1) {
                        vezDoJogador = 1;
                        vezLabel.setText("Sua vez!");
                        conexao.setMostrar(); //Reseta o valor de mostrar.

                        JOptionPane.showMessageDialog(null, "Sua vez!");

                        /* Se mudou de vez, quer dizer que o oponente errou,
                         e as mudanças na interface devem ser desfeitas. */
                        desfaser(pontuar[0]);
                        desfaser(pontuar[1]);
                    } else if (conexao.getMostrar() == -2) //Oponente pontuou.
                    {
                        pontosSeu++; //Eleva os pontos do oponente.

                        desativados += 2; //Dois blocos foram encontrados.

                        desativar[pontuar[0]] = desativar[pontuar[1]] = 1; //Desativa o clique deles.

                        oponentesPontosLabel.setText("Oponente = " + pontosSeu);

                        conexao.setMostrar();
                    } else if (conexao.getMostrar() == 99) //Não faz nada.
                    {
                    } else //Atualiza a interface para mostrar o que o oponente clicou.
                    {
                        conexao.setMostrar();

                        pontuar[x] = conexao.getClick(); //Guarda o que foi clicado.

                        mostrarImg(pontuar[x]); //Atualiza a interface.

                        x++; //1 = Oponente revelou uma imagem

                        if (x == 2) //Oponente terminou, a jogada reseta.
                        {
                            x = 0;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Erro no novo código.");
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        area1 = new javax.swing.JLabel();
        area2 = new javax.swing.JLabel();
        area3 = new javax.swing.JLabel();
        area4 = new javax.swing.JLabel();
        area5 = new javax.swing.JLabel();
        area6 = new javax.swing.JLabel();
        area7 = new javax.swing.JLabel();
        area8 = new javax.swing.JLabel();
        area9 = new javax.swing.JLabel();
        area10 = new javax.swing.JLabel();
        area11 = new javax.swing.JLabel();
        area12 = new javax.swing.JLabel();
        area13 = new javax.swing.JLabel();
        area14 = new javax.swing.JLabel();
        area15 = new javax.swing.JLabel();
        area16 = new javax.swing.JLabel();
        area17 = new javax.swing.JLabel();
        area18 = new javax.swing.JLabel();
        area19 = new javax.swing.JLabel();
        area20 = new javax.swing.JLabel();
        area21 = new javax.swing.JLabel();
        area22 = new javax.swing.JLabel();
        area23 = new javax.swing.JLabel();
        area24 = new javax.swing.JLabel();
        pontosLabel = new javax.swing.JLabel();
        meusPontosLabel = new javax.swing.JLabel();
        oponentesPontosLabel = new javax.swing.JLabel();
        vezLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        arquivoMenu = new javax.swing.JMenu();
        novoJogoMenu = new javax.swing.JMenuItem();
        conectarMenu = new javax.swing.JMenuItem();
        sairMenu = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Memoria");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        area1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area1MouseClicked(evt);
            }
        });
        getContentPane().add(area1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        area2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area2MouseClicked(evt);
            }
        });
        getContentPane().add(area2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, -1, -1));

        area3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area3MouseClicked(evt);
            }
        });
        getContentPane().add(area3, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, -1, -1));

        area4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area4MouseClicked(evt);
            }
        });
        getContentPane().add(area4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 0, -1, -1));

        area5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area5MouseClicked(evt);
            }
        });
        getContentPane().add(area5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, -1, -1));

        area6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area6MouseClicked(evt);
            }
        });
        getContentPane().add(area6, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, -1, -1));

        area7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area7MouseClicked(evt);
            }
        });
        getContentPane().add(area7, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, -1, -1));

        area8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area8MouseClicked(evt);
            }
        });
        getContentPane().add(area8, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 100, -1, -1));

        area9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area9MouseClicked(evt);
            }
        });
        getContentPane().add(area9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 200, -1, -1));

        area10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area10MouseClicked(evt);
            }
        });
        getContentPane().add(area10, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, -1, -1));

        area11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area11MouseClicked(evt);
            }
        });
        getContentPane().add(area11, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 200, -1, -1));

        area12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area12MouseClicked(evt);
            }
        });
        getContentPane().add(area12, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 200, -1, -1));

        area13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area13MouseClicked(evt);
            }
        });
        getContentPane().add(area13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, -1, -1));

        area14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area14MouseClicked(evt);
            }
        });
        getContentPane().add(area14, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, -1, -1));

        area15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area15MouseClicked(evt);
            }
        });
        getContentPane().add(area15, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, -1, -1));

        area16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area16MouseClicked(evt);
            }
        });
        getContentPane().add(area16, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, -1, -1));

        area17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area17MouseClicked(evt);
            }
        });
        getContentPane().add(area17, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, -1, -1));

        area18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area18MouseClicked(evt);
            }
        });
        getContentPane().add(area18, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 400, -1, -1));

        area19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area19MouseClicked(evt);
            }
        });
        getContentPane().add(area19, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 400, -1, -1));

        area20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area20MouseClicked(evt);
            }
        });
        getContentPane().add(area20, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 400, -1, -1));

        area21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area21MouseClicked(evt);
            }
        });
        getContentPane().add(area21, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, -1, -1));

        area22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area22MouseClicked(evt);
            }
        });
        getContentPane().add(area22, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 500, -1, -1));

        area23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area23MouseClicked(evt);
            }
        });
        getContentPane().add(area23, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 500, -1, -1));

        area24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagem/memoria.jpg"))); // NOI18N
        area24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                area24MouseClicked(evt);
            }
        });
        getContentPane().add(area24, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 500, -1, -1));

        pontosLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        pontosLabel.setText("Pontos");
        getContentPane().add(pontosLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 610, -1, -1));

        meusPontosLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        meusPontosLabel.setText("Você = 0");
        getContentPane().add(meusPontosLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 640, -1, -1));

        oponentesPontosLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        oponentesPontosLabel.setText("Oponente = 0");
        getContentPane().add(oponentesPontosLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 670, -1, -1));

        vezLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        vezLabel.setToolTipText("");
        getContentPane().add(vezLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 670, -1, -1));

        arquivoMenu.setText("Arquivo");
        arquivoMenu.setToolTipText("");

        novoJogoMenu.setText("Novo jogo");
        novoJogoMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoJogoMenuActionPerformed(evt);
            }
        });
        arquivoMenu.add(novoJogoMenu);

        conectarMenu.setText("Conectar-se");
        conectarMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conectarMenuActionPerformed(evt);
            }
        });
        arquivoMenu.add(conectarMenu);

        sairMenu.setText("Sair");
        sairMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairMenuActionPerformed(evt);
            }
        });
        arquivoMenu.add(sairMenu);

        jMenuBar1.add(arquivoMenu);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void novoJogoMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoJogoMenuActionPerformed

        try {
            // Bloqueia o menu para nao ser utilizado novamente.
            conectarMenu.setEnabled(false);
            novoJogoMenu.setEnabled(false);

            //Registry registry = LocateRegistry.getRegistry();

            //conexao = (Comunicacao) registry.lookup("Server"); //Conecta com o servidor registrado como Server.
            
           ws = new ComunicacaoWS_Service();
            
           conexao = ws.getPort(ComunicacaoWS.class);

            System.out.println("Servidor encontrado.\n");

            souServer = 1; //Você é o servidor -> com o RMI, isso agora é falso. Virou uma variável de controle.

            //vezLabel.setText("Esperando..."); //Esperando conexao.

            //quantidade(); //Parcialmente implementado, possível uso para vários jogadores.
            tabuleiro = conexao.getTabuleiro(); //Recebe o tabuleiro.

            vezDoJogador = 1; //O servidor começa.

            vezLabel.setText("Sua vez!");

            Thread Th = new Thread(this); //Inicia a thread de comunicação, o run acima.
            Th.start();
        } catch (Exception e) {
            System.out.println("Servidor não encontrado.\n");
            e.printStackTrace();
            System.exit(1);
        }
    }//GEN-LAST:event_novoJogoMenuActionPerformed

    private void conectarMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conectarMenuActionPerformed

        // Bloqueia o menu para não ser utilizado novamente.
        novoJogoMenu.setEnabled(false);
        conectarMenu.setEnabled(false);

        souServer = 2; //2 = cliente

        try {
            //Registry registry = LocateRegistry.getRegistry();

            //conexao = (Comunicacao) registry.lookup("Server");
            
            ws = new ComunicacaoWS_Service();
            
           conexao = ws.getPort(ComunicacaoWS.class);

            System.out.println("Servidor encontrado.\n");

            tabuleiro = conexao.getTabuleiro(); //Recebe o tabuleiro.

            Thread Th = new Thread(this); //Inicia a thread de comunicação, o run acima.
            Th.start();
        } catch (Exception e) {
            System.out.println("Servidor não encontrado.\n");
            System.exit(1);
        }
    }//GEN-LAST:event_conectarMenuActionPerformed

    private void sairMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairMenuActionPerformed
        int decisao = JOptionPane.showConfirmDialog(null, "Tem certeza que quer sair?");
        if (decisao == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_sairMenuActionPerformed

    private void area1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area1MouseClicked

        /* Todos os códigos de tratamento de clique nas labels do tabuleiro são idênticos,
         então somente este será comentado. */
        if (vezDoJogador == 0 || desativar[1] == 1) //Se não for a sua vez ou estiver desativado.
        {
            return;
        }

        if (click == 0) //Foi o primeiro clique, ou seja, sua primeira jogada.
        {
            click++;

            mostrarImg(1); //Mostra a imagem no bloco 1.

            guardarClick(1); //Manda o bloco clicado para o oponente.

            pontuar[0] = 1; //Guarda o bloco clicado.
        } else if (pontuar[0] == 1) //Você clicou no mesmo bloco na segunda jogada.
        {
            return;
        } else //Segunda jogada.
        {
            click = 0; //Reseta o clique.
            //vezDoJogador = 0;

            mostrarImg(1);

            guardarClick(1);

            pontuar[1] = 1;

            checarPontos(); //Verifica se pontuou.
        }
    }//GEN-LAST:event_area1MouseClicked

    private void area2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area2MouseClicked
        if (vezDoJogador == 0 || desativar[2] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(2);

            guardarClick(2);

            pontuar[0] = 2;
        } else if (pontuar[0] == 2) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(2);

            guardarClick(2);

            pontuar[1] = 2;

            checarPontos();
        }
    }//GEN-LAST:event_area2MouseClicked

    private void area3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area3MouseClicked
        if (vezDoJogador == 0 || desativar[3] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(3);

            guardarClick(3);

            pontuar[0] = 3;
        } else if (pontuar[0] == 3) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(3);

            guardarClick(3);

            pontuar[1] = 3;

            checarPontos();
        }
    }//GEN-LAST:event_area3MouseClicked

    private void area4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area4MouseClicked
        if (vezDoJogador == 0 || desativar[4] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(4);

            guardarClick(4);

            pontuar[0] = 4;
        } else if (pontuar[0] == 4) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(4);

            guardarClick(4);

            pontuar[1] = 4;

            checarPontos();
        }
    }//GEN-LAST:event_area4MouseClicked

    private void area5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area5MouseClicked
        if (vezDoJogador == 0 || desativar[5] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(5);

            guardarClick(5);

            pontuar[0] = 5;
        } else if (pontuar[0] == 5) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(5);

            guardarClick(5);

            pontuar[1] = 5;

            checarPontos();
        }
    }//GEN-LAST:event_area5MouseClicked

    private void area6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area6MouseClicked
        if (vezDoJogador == 0 || desativar[6] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(6);

            guardarClick(6);

            pontuar[0] = 6;
        } else if (pontuar[0] == 6) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(6);

            guardarClick(6);

            pontuar[1] = 6;

            checarPontos();
        }
    }//GEN-LAST:event_area6MouseClicked

    private void area7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area7MouseClicked
        if (vezDoJogador == 0 || desativar[7] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(7);

            guardarClick(7);

            pontuar[0] = 7;
        } else if (pontuar[0] == 7) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(7);

            guardarClick(7);

            pontuar[1] = 7;

            checarPontos();
        }
    }//GEN-LAST:event_area7MouseClicked

    private void area8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area8MouseClicked
        if (vezDoJogador == 0 || desativar[8] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(8);

            guardarClick(8);

            pontuar[0] = 8;
        } else if (pontuar[0] == 8) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(8);

            guardarClick(8);

            pontuar[1] = 8;

            checarPontos();
        }
    }//GEN-LAST:event_area8MouseClicked

    private void area9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area9MouseClicked
        if (vezDoJogador == 0 || desativar[9] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(9);

            guardarClick(9);

            pontuar[0] = 9;
        } else if (pontuar[0] == 9) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(9);

            guardarClick(9);

            pontuar[1] = 9;

            checarPontos();
        }
    }//GEN-LAST:event_area9MouseClicked

    private void area10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area10MouseClicked
        if (vezDoJogador == 0 || desativar[10] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(10);

            guardarClick(10);

            pontuar[0] = 10;
        } else if (pontuar[0] == 10) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(10);

            guardarClick(10);

            pontuar[1] = 10;

            checarPontos();
        }
    }//GEN-LAST:event_area10MouseClicked

    private void area11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area11MouseClicked
        if (vezDoJogador == 0 || desativar[11] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(11);

            guardarClick(11);

            pontuar[0] = 11;
        } else if (pontuar[0] == 11) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(11);

            guardarClick(11);

            pontuar[1] = 11;

            checarPontos();
        }
    }//GEN-LAST:event_area11MouseClicked

    private void area12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area12MouseClicked
        if (vezDoJogador == 0 || desativar[12] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(12);

            guardarClick(12);

            pontuar[0] = 12;
        } else if (pontuar[0] == 12) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(12);

            guardarClick(12);

            pontuar[1] = 12;

            checarPontos();
        }
    }//GEN-LAST:event_area12MouseClicked

    private void area13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area13MouseClicked
        if (vezDoJogador == 0 || desativar[13] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(13);

            guardarClick(13);

            pontuar[0] = 13;
        } else if (pontuar[0] == 13) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(13);

            guardarClick(13);

            pontuar[1] = 13;

            checarPontos();
        }
    }//GEN-LAST:event_area13MouseClicked

    private void area14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area14MouseClicked
        if (vezDoJogador == 0 || desativar[14] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(14);

            guardarClick(14);

            pontuar[0] = 14;
        } else if (pontuar[0] == 14) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(14);

            guardarClick(14);

            pontuar[1] = 14;

            checarPontos();
        }
    }//GEN-LAST:event_area14MouseClicked

    private void area15MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area15MouseClicked
        if (vezDoJogador == 0 || desativar[15] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(15);

            guardarClick(15);

            pontuar[0] = 15;
        } else if (pontuar[0] == 15) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(15);

            guardarClick(15);

            pontuar[1] = 15;

            checarPontos();
        }
    }//GEN-LAST:event_area15MouseClicked

    private void area16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area16MouseClicked
        if (vezDoJogador == 0 || desativar[16] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(16);

            guardarClick(16);

            pontuar[0] = 16;
        } else if (pontuar[0] == 16) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(16);

            guardarClick(16);

            pontuar[1] = 16;

            checarPontos();
        }
    }//GEN-LAST:event_area16MouseClicked

    private void area17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area17MouseClicked
        if (vezDoJogador == 0 || desativar[17] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(17);

            guardarClick(17);

            pontuar[0] = 17;
        } else if (pontuar[0] == 17) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(17);

            guardarClick(17);

            pontuar[1] = 17;

            checarPontos();
        }
    }//GEN-LAST:event_area17MouseClicked

    private void area18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area18MouseClicked
        if (vezDoJogador == 0 || desativar[18] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(18);

            guardarClick(18);

            pontuar[0] = 18;
        } else if (pontuar[0] == 18) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(18);

            guardarClick(18);

            pontuar[1] = 18;

            checarPontos();
        }
    }//GEN-LAST:event_area18MouseClicked

    private void area19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area19MouseClicked
        if (vezDoJogador == 0 || desativar[19] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(19);

            guardarClick(19);

            pontuar[0] = 19;
        } else if (pontuar[0] == 19) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(19);

            guardarClick(19);

            pontuar[1] = 19;

            checarPontos();
        }
    }//GEN-LAST:event_area19MouseClicked

    private void area20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area20MouseClicked
        if (vezDoJogador == 0 || desativar[20] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(20);

            guardarClick(20);

            pontuar[0] = 20;
        } else if (pontuar[0] == 20) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(20);

            guardarClick(20);

            pontuar[1] = 20;

            checarPontos();
        }
    }//GEN-LAST:event_area20MouseClicked

    private void area21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area21MouseClicked
        if (vezDoJogador == 0 || desativar[21] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(21);

            guardarClick(21);

            pontuar[0] = 21;
        } else if (pontuar[0] == 21) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(21);

            guardarClick(21);

            pontuar[1] = 21;

            checarPontos();
        }
    }//GEN-LAST:event_area21MouseClicked

    private void area22MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area22MouseClicked
        if (vezDoJogador == 0 || desativar[22] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(22);

            guardarClick(22);

            pontuar[0] = 22;
        } else if (pontuar[0] == 22) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(22);

            guardarClick(22);

            pontuar[1] = 22;

            checarPontos();
        }
    }//GEN-LAST:event_area22MouseClicked

    private void area23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area23MouseClicked
        if (vezDoJogador == 0 || desativar[23] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(23);

            guardarClick(23);

            pontuar[0] = 23;
        } else if (pontuar[0] == 23) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(23);

            guardarClick(23);

            pontuar[1] = 23;

            checarPontos();
        }
    }//GEN-LAST:event_area23MouseClicked

    private void area24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_area24MouseClicked
        if (vezDoJogador == 0 || desativar[24] == 1) {
            return;
        }

        if (click == 0) {
            click++;

            mostrarImg(24);

            guardarClick(24);

            pontuar[0] = 24;
        } else if (pontuar[0] == 24) {
            return;
        } else {
            click = 0;
            //vezDoJogador = 0;

            mostrarImg(24);

            guardarClick(24);

            pontuar[1] = 24;

            checarPontos();
        }
    }//GEN-LAST:event_area24MouseClicked

    public static void main(String args[]) {

        for (int y = 0; y < 25; y++) //Inicia o vetor que guarda os blocos desativados.
        {
            desativar[y] = 0;
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MemoriaInterface().setVisible(true);
            }
        });
    }

    public static void quantidade() //Embora o jogador escolha 2 ou 3, sempre será 2.
    {
        while (quantJogadores != 2 && quantJogadores != 3) {
            String decisao = JOptionPane.showInputDialog(null, "Deseja criar um jogo"
                    + "para 2 ou 3 jogadores? (entre com 2 ou 3)");

            try {
                //quantJogadores = Integer.parseInt(decisao);
                quantJogadores = 2;
            } catch (Exception e) {

            }
        }
    }

    public static void guardarClick(int click) {
        // Envia o bloco clicado para o cliente ou servidor.
        try {
            conexao.tratar("jogada " + click);

            if (vezDoJogador == 0) {
                click = -1;

                conexao.tratar("jogada " + click);
            }

        } catch (Exception e) {
            System.out.println("Erro ao comunicar a jogada.");
            System.exit(1);
        }
    }

    public static void mostrarImg(int valor) {
        ImageIcon img = getImgArea(valor); //Pega a imagem.

        switch (valor) //Coloca a imagem na área clicada.
        {
            case 1:
                area1.setIcon(img);
                break;
            case 2:
                area2.setIcon(img);
                break;
            case 3:
                area3.setIcon(img);
                break;
            case 4:
                area4.setIcon(img);
                break;
            case 5:
                area5.setIcon(img);
                break;
            case 6:
                area6.setIcon(img);
                break;
            case 7:
                area7.setIcon(img);
                break;
            case 8:
                area8.setIcon(img);
                break;
            case 9:
                area9.setIcon(img);
                break;
            case 10:
                area10.setIcon(img);
                break;
            case 11:
                area11.setIcon(img);
                break;
            case 12:
                area12.setIcon(img);
                break;
            case 13:
                area13.setIcon(img);
                break;
            case 14:
                area14.setIcon(img);
                break;
            case 15:
                area15.setIcon(img);
                break;
            case 16:
                area16.setIcon(img);
                break;
            case 17:
                area17.setIcon(img);
                break;
            case 18:
                area18.setIcon(img);
                break;
            case 19:
                area19.setIcon(img);
                break;
            case 20:
                area20.setIcon(img);
                break;
            case 21:
                area21.setIcon(img);
                break;
            case 22:
                area22.setIcon(img);
                break;
            case 23:
                area23.setIcon(img);
                break;
            case 24:
                area24.setIcon(img);
                break;
            default:
                break;
        }
    }

    public static ImageIcon getImgArea(int valor) {
        ImageIcon img;

        switch (tabuleiro.getValor(valor)) //Verifica o valor na matriz e atribui uma imagem.
        {
            case 1:
                return img = new ImageIcon("Imgs/ajax.jpg");
            case 2:
                return img = new ImageIcon("Imgs/java.jpg");
            case 3:
                return img = new ImageIcon("Imgs/javascript.jpg");
            case 4:
                return img = new ImageIcon("Imgs/jquery.jpg");
            case 5:
                return img = new ImageIcon("Imgs/json.jpg");
            case 6:
                return img = new ImageIcon("Imgs/nodejs.jpg");
            case 7:
                return img = new ImageIcon("Imgs/peertopeer.jpg");
            case 8:
                return img = new ImageIcon("Imgs/rmi.jpg");
            case 9:
                return img = new ImageIcon("Imgs/socket.jpg");
            case 10:
                return img = new ImageIcon("Imgs/ssl.jpg");
            case 11:
                return img = new ImageIcon("Imgs/uddi.jpg");
            default:
                return img = new ImageIcon("Imgs/webservices.jpg");
        }
    }

    public static void checarPontos() //Verifica se pontuou.
    {
        //Se os dois guardados forem iguais, acertou.
        if (tabuleiro.getValor(pontuar[0]) == tabuleiro.getValor(pontuar[1])) {
            desativados += 2; //Dois blocos serão desativados.

            pontosMeu++; //Fiz 1 ponto.

            checaVitoria(); //Verifica se acabou o jogo.

            JOptionPane.showMessageDialog(null, "Ponto! Jogue novamente.");
            vezDoJogador = 1;

            desativar[pontuar[0]] = desativar[pontuar[1]] = 1; //Desativa os dois blocos.

            meusPontosLabel.setText("Você = " + pontosMeu);

            try {
                conexao.tratar("Ponto"); //Avisa ao seu oponente que você pontuou.
            } catch (Exception e) {
                System.out.println("Erro ao comunicar ponto.");
                System.exit(1);
            }
        } else //Errou.
        {
            try { //Avisa ao seu oponente que é a vez dele.
                conexao.tratar("vez");
            } catch (Exception e) {
                System.out.println("Erro ao passar a vez.");
                System.exit(1);
            }
            JOptionPane.showMessageDialog(null, "Errado!");

            desfaser(pontuar[0]); //Reseta a interface.
            desfaser(pontuar[1]); //Reseta a interface.

            vezDoJogador = 0; //Acabou a sua vez.
        }
    }

    public static void checaVitoria() {
        if (desativados == 24) //Se todos os blocos foram desativados.
        {
            if (pontosMeu >= pontosSeu) {
                JOptionPane.showMessageDialog(null, "Você venceu!");
                System.exit(0);
            } else {
                JOptionPane.showMessageDialog(null, "Você perdeu!");
                System.exit(0);
            }
        }
    }

    public static void desfaser(int area) //Esconde as imagens reveladas.
    {
        ImageIcon img = new ImageIcon("Imgs/Memoria.jpg");

        switch (area) {
            case 1:
                area1.setIcon(img);
                break;
            case 2:
                area2.setIcon(img);
                break;
            case 3:
                area3.setIcon(img);
                break;
            case 4:
                area4.setIcon(img);
                break;
            case 5:
                area5.setIcon(img);
                break;
            case 6:
                area6.setIcon(img);
                break;
            case 7:
                area7.setIcon(img);
                break;
            case 8:
                area8.setIcon(img);
                break;
            case 9:
                area9.setIcon(img);
                break;
            case 10:
                area10.setIcon(img);
                break;
            case 11:
                area11.setIcon(img);
                break;
            case 12:
                area12.setIcon(img);
                break;
            case 13:
                area13.setIcon(img);
                break;
            case 14:
                area14.setIcon(img);
                break;
            case 15:
                area15.setIcon(img);
                break;
            case 16:
                area16.setIcon(img);
                break;
            case 17:
                area17.setIcon(img);
                break;
            case 18:
                area18.setIcon(img);
                break;
            case 19:
                area19.setIcon(img);
                break;
            case 20:
                area20.setIcon(img);
                break;
            case 21:
                area21.setIcon(img);
                break;
            case 22:
                area22.setIcon(img);
                break;
            case 23:
                area23.setIcon(img);
                break;
            case 24:
                area24.setIcon(img);
                break;
            default:
                break;
        }
    }

    private static int quantJogadores;
    private static int vezDoJogador = 0;
    private static int click = 0;
    private static int pontosMeu = 0;
    private static int pontosSeu = 0;
    private static int desativados = 0;

    private static int[] pontuar = new int[2];
    private static int[] desativar = new int[25];

    //private static Comunicacao conexao;
    
    private static ComunicacaoWS_Service ws;
    
    private static ComunicacaoWS conexao;

    //private static Cliente cliente;
    //private static Servidor servidor;
    private static int souServer = 0;

    private static Tabuleiro tabuleiro;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JLabel area1;
    private static javax.swing.JLabel area10;
    private static javax.swing.JLabel area11;
    private static javax.swing.JLabel area12;
    private static javax.swing.JLabel area13;
    private static javax.swing.JLabel area14;
    private static javax.swing.JLabel area15;
    private static javax.swing.JLabel area16;
    private static javax.swing.JLabel area17;
    private static javax.swing.JLabel area18;
    private static javax.swing.JLabel area19;
    private static javax.swing.JLabel area2;
    private static javax.swing.JLabel area20;
    private static javax.swing.JLabel area21;
    private static javax.swing.JLabel area22;
    private static javax.swing.JLabel area23;
    private static javax.swing.JLabel area24;
    private static javax.swing.JLabel area3;
    private static javax.swing.JLabel area4;
    private static javax.swing.JLabel area5;
    private static javax.swing.JLabel area6;
    private static javax.swing.JLabel area7;
    private static javax.swing.JLabel area8;
    private static javax.swing.JLabel area9;
    private javax.swing.JMenu arquivoMenu;
    private javax.swing.JMenuItem conectarMenu;
    private javax.swing.JMenuBar jMenuBar1;
    private static javax.swing.JLabel meusPontosLabel;
    private javax.swing.JMenuItem novoJogoMenu;
    private static javax.swing.JLabel oponentesPontosLabel;
    private javax.swing.JLabel pontosLabel;
    private javax.swing.JMenuItem sairMenu;
    private static javax.swing.JLabel vezLabel;
    // End of variables declaration//GEN-END:variables
}
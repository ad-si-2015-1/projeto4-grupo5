package memoria;

import java.io.Serializable;
import java.util.*;

public class Tabuleiro implements Serializable //Pode enviar o objeto pelo socket.
{

    private int[][] tabuleiro; //Matriz que controlará a interface da memória.

    public Tabuleiro() {
        tabuleiro = new int[6][4];

        preencher();
    }

    private void preencher() //Cria uma disposição aleatória dos elementos da matriz.
    {
        Random random = new Random();

        ArrayList<Integer> randomList = listaRandom(); //Lista de apoio a aleatoridade.

        int linha = 0;
        int coluna = 0;

        while (!randomList.isEmpty()) //Enquanto a lista nao estiver vazia...
        {
            //Sorteia um valor da lista e coloca na matriz.
            tabuleiro[linha][coluna] = randomList.remove(random.nextInt(randomList.size()));

            //Movimentação na matriz.
            coluna++;

            if (coluna == 4) {
                linha++;
                coluna = 0;
            }
        }
    }

    private ArrayList<Integer> listaRandom() {
        ArrayList<Integer> randomList = new ArrayList<Integer>();

        /*
         * Preenche a lista com valores de 1 a 12.
         * Cada número representa uma imagem a ser apresentada na interface.
         * Como cada imagem tem 1 par, os valores são colocados duas vezes.
         * Esta lista garante que o random() colocará apenas valores de 1 a 12 e
         * os repetirá somente uma vez.
         */
        for (int x = 1; x <= 12; x++) {
            randomList.add(x);
            randomList.add(x);
        }

        return randomList;
    }

    public int[][] getTabuleiro() {
        return tabuleiro;
    }

    public int getValor(int posicao) {
        switch (posicao) {
            case 1:
                return tabuleiro[0][0];
            case 2:
                return tabuleiro[0][1];
            case 3:
                return tabuleiro[0][2];
            case 4:
                return tabuleiro[0][3];
            case 5:
                return tabuleiro[1][0];
            case 6:
                return tabuleiro[1][1];
            case 7:
                return tabuleiro[1][2];
            case 8:
                return tabuleiro[1][3];
            case 9:
                return tabuleiro[2][0];
            case 10:
                return tabuleiro[2][1];
            case 11:
                return tabuleiro[2][2];
            case 12:
                return tabuleiro[2][3];
            case 13:
                return tabuleiro[3][0];
            case 14:
                return tabuleiro[3][1];
            case 15:
                return tabuleiro[3][2];
            case 16:
                return tabuleiro[3][3];
            case 17:
                return tabuleiro[4][0];
            case 18:
                return tabuleiro[4][1];
            case 19:
                return tabuleiro[4][2];
            case 20:
                return tabuleiro[4][3];
            case 21:
                return tabuleiro[5][0];
            case 22:
                return tabuleiro[5][1];
            case 23:
                return tabuleiro[5][2];
            default:
                return tabuleiro[5][3];
        }
    }
}
##Leiame

A versão estável do projeto encontra-se nos códigos dos arquivos zipados. Para que seja executado de forma correta o servidor é necessário modificar o caminho do WSDL, para a máquina que funciona como localhost, no caso, a que executa a aplicação. O arquivo que deve ser modificado segue abaixo:

Pacote build>generated>jax-wsCache>service>ClienteWS>wserver>ComunicacaoWS_Service.java

Na linha 20 e 33, altere o caminho. Se for executar no windows, apenas exclua o nome do usuário da máquina, que vem após o /users/NOMEDOUSUÁRIO.


Execução:
1. Executar a classe Servidor.java;
2. Executar a classe MemoriaInterface.java duas vezes (uma tela para cada jogador);
3. Na tela do primeiro jogador conectado, clicar em Arquivo > Novo jogo;
4. Na tela do segundo jogador conectado, clicar em Arquivo > Conectar-se;
5. O primeiro jogador escolhe as peças.